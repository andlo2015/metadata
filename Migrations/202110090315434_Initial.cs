﻿namespace ControlMetadata.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Initial : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Actividades",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UsuarioId = c.Int(nullable: false),
                        Descripcion = c.String(nullable: false, maxLength: 500),
                        Estado = c.Boolean(nullable: false),
                        Nombre = c.String(nullable: false, maxLength: 100),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Usuarios", t => t.UsuarioId, cascadeDelete: true)
                .Index(t => t.UsuarioId);
            
            CreateTable(
                "dbo.ActividadMetadata",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ActividadId = c.Int(nullable: false),
                        ObjetoId = c.Int(nullable: false),
                        Descripcion = c.String(nullable: false, maxLength: 500),
                        Estado = c.Boolean(nullable: false),
                        Numero = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Actividades", t => t.ActividadId, cascadeDelete: true)
                .Index(t => t.ActividadId)
                .Index(t => new { t.ObjetoId, t.Numero }, unique: true);
            
            CreateTable(
                "dbo.Usuarios",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Username = c.String(nullable: false, maxLength: 100),
                        Password = c.String(nullable: false, maxLength: 100),
                        Estado = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.Username, unique: true);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Actividades", "UsuarioId", "dbo.Usuarios");
            DropForeignKey("dbo.ActividadMetadata", "ActividadId", "dbo.Actividades");
            DropIndex("dbo.Usuarios", new[] { "Username" });
            DropIndex("dbo.ActividadMetadata", new[] { "ObjetoId", "Numero" });
            DropIndex("dbo.ActividadMetadata", new[] { "ActividadId" });
            DropIndex("dbo.Actividades", new[] { "UsuarioId" });
            DropTable("dbo.Usuarios");
            DropTable("dbo.ActividadMetadata");
            DropTable("dbo.Actividades");
        }
    }
}
