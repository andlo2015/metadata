﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ControlMetadata.Model.Entities
{
    /// <summary>
    /// Información de la Actividad
    /// </summary>
    [Table("Actividades")]
    public class Actividad
    {

        /// <summary>
        /// Id del registro
        /// </summary>
        public int Id { get; set; } = 0;

        /// <summary>
        /// Usuario al que pertenece la actividad
        /// </summary>
        [Required(AllowEmptyStrings = false, ErrorMessage = "{0} - Obligatorio")]
        public int UsuarioId { get; set; } = 0;

        /// <summary>
        /// Descripcion de lo que se va a realizar en el sprint
        /// </summary>
        [Required(AllowEmptyStrings = false, ErrorMessage = "{0} - Obligatorio")]
        [MaxLength(500, ErrorMessage = "{0} - Maximo {1} caracteres")]
        public string Descripcion { get; set; } = "";

        /// <summary>
        /// Estado del registro
        /// </summary>
        [Required(AllowEmptyStrings = false, ErrorMessage = "{0} - Obligatorio")]
        public bool Estado { get; set; } = true;

        /// <summary>
        /// Nombre de la actividad. Puede ser la tarea principal
        /// </summary>
        [Required(AllowEmptyStrings = false, ErrorMessage = "{0} - Obligatorio")]
        [MaxLength(100, ErrorMessage = "{0} - Maximo {1} caracteres")]
        public string Nombre { get; set; } = "";

        /// <summary>
        /// Detalle de la metadata creada en la actividad
        /// </summary>
        public List<Metadata> Detalle { get; set; } = new List<Metadata>();


    }
}
