﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ControlMetadata.Model.Entities
{
    public class Usuario
    {

        public int Id { get; set; } = 0;

        [Required(AllowEmptyStrings = false, ErrorMessage = "{0} - Obligatorio")]
        [MaxLength(100, ErrorMessage = "{0} - Maximo {1} caracteres")]
        [EmailAddress(ErrorMessage = "{0} - No es un email válido")]
        public string Username { get; set; } = "";

        [Required(AllowEmptyStrings = false, ErrorMessage = "{0} - Obligatorio")]
        [MaxLength(100, ErrorMessage = "{0} - Maximo {1} caracteres")]
        public string Password { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "{0} - Obligatorio")]
        public bool Estado { get; set; } = true;


        public List<Actividad> Sprints { get; set; }

    }
}
