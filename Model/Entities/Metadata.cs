﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static ControlMetadata.Enumerador;

namespace ControlMetadata.Model.Entities
{
    /// <summary>
    /// Indica cada registro de metadata creado en la actividad
    /// </summary>
    [Table("ActividadMetadata")]
    public class Metadata
    {

        /// <summary>
        /// Id del registro
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Id de la actividad al que pertenece
        /// </summary>
        public int ActividadId { get; set; }

        /// <summary>
        /// Tipo de objeto
        /// </summary>
        [Required(AllowEmptyStrings =false,ErrorMessage ="{0} - Obligatorio")]
        public ObjetosTopaz ObjetoId { get; set; }

        /// <summary>
        /// Descripcion para que se creo 
        /// </summary>
        [Required(AllowEmptyStrings = false, ErrorMessage = "{0} - Obligatorio")]
        [MaxLength(500, ErrorMessage = "{0} - Maximo {1} caracteres")]
        public string Descripcion { get; set; } = "";

        /// <summary>
        /// Estado del registro
        /// </summary>
        [Required(AllowEmptyStrings = false, ErrorMessage = "{0} - Obligatorio")]
        public bool Estado { get; set; } = true;

        /// <summary>
        /// Numero del objeto
        /// </summary>
        [Required(AllowEmptyStrings = false, ErrorMessage = "{0} - Obligatorio")]
        public int Numero { get; set; } = 0;

       
    }
}
