﻿using ControlMetadata.Model.Entities;
using System.Linq;

namespace ControlMetadata.Model.Repositories
{
    public class UsuarioRepository
    {

        public Contexto _ctx;

        /// <summary>
        /// Constructor de la clase
        /// </summary>
        /// <param name="ctx">Contexto de datos</param>
        public UsuarioRepository(Contexto ctx)
        {
            this._ctx = ctx;
        }

        /// <summary>
        /// Autentica un usuario
        /// </summary>
        public Usuario Login(string usuario,string clave)
        {
            return _ctx.Usuarios.FirstOrDefault(u => u.Username==usuario 
                    && u.Password==clave && u.Estado == true);
        }


    }
}
