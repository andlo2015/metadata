﻿using ControlMetadata.Model.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;

namespace ControlMetadata.Model.Repositories
{
    /// <summary>
    /// Repositorio para gestionar las acciones con la metadata
    /// </summary>
    public class MetadataRepository
    {

        public Contexto _ctx;

        /// <summary>
        /// Constructor de la clase
        /// </summary>
        /// <param name="ctx">Contexto de datos</param>
        public MetadataRepository(Contexto ctx)
        {
            this._ctx = ctx;
        }

        /// <summary>
        /// Recupera el detalle de registros por el Id de la actividad
        /// </summary>
        /// <param name="actividadId">Id de la actividad</param>
        /// <returns></returns>
        public IEnumerable<Metadata> GetByActividadId(int actividadId)
        {
            return _ctx.Metadata.Where(m => m.ActividadId == actividadId && m.Estado==true).ToList();
        }

        /// <summary>
        /// Permite agregar o modificar un registro de metadata
        /// </summary>
        /// <param name="metadata">Informacion de la metadata</param>
        /// <returns></returns>
        public async Task<Metadata> Save(Metadata metadata)
        {
            var metadataDb = await _ctx.Metadata.FirstOrDefaultAsync(m => m.Numero == metadata.Numero && m.ObjetoId==metadata.ObjetoId);

            //Valida que NO duplique el Identificador de TOPAZ.
            if (metadataDb!=null && metadataDb?.ActividadId != metadata.ActividadId)
                throw new Exception("El número de objeto ingresado ya está asignado");
            //Nuevo registro
            if (metadataDb == null)
            {
                _ctx.Metadata.Add(metadata);
            }
            //Edicion
            else
            {
                metadataDb.Descripcion = metadata.Descripcion;
                _ctx.Entry(metadataDb).State = EntityState.Modified;
            }
            await _ctx.SaveChangesAsync();
            return metadata;
        }

        /// <summary>
        /// Permite eliminar un registro de metadata
        /// </summary>
        /// <param name="metadata"></param>
        public void Delete(Metadata metadata)
        {
            _ctx.Metadata.Remove(metadata);
            _ctx.SaveChanges();
        }


    }
}
