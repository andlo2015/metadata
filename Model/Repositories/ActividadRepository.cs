﻿using ControlMetadata.Model.Entities;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;

namespace ControlMetadata.Model.Repositories
{
    /// <summary>
    /// Repositorio para gestionar las actividades
    /// </summary>
    public class ActividadRepository
    {

        public Contexto _ctx;

        /// <summary>
        /// Constructor de la clase
        /// </summary>
        public ActividadRepository(Contexto ctx)
        {
            this._ctx = ctx;
        }

        /// <summary>
        /// Recupera las actividades de un Usuario
        /// </summary>
        /// <param name="usuarioId">Id del usuario a consultar</param>
        /// <returns></returns>
        public IEnumerable<Actividad> GetByUsuarioId(int usuarioId)
        {
            return _ctx.Actividades.Where(act => act.UsuarioId == usuarioId && act.Estado==true)
                 .OrderBy(m=>m.Nombre).ToList();
        }

        /// <summary>
        /// Permite agregar o modificar una actividad
        /// </summary>
        /// <param name="actividad">Actividad a guardar</param>
        /// <returns></returns>
        public async Task<Actividad> Save(Actividad actividad)
        {
            var actividadDb = await _ctx.Actividades.FirstOrDefaultAsync(act => act.Id == actividad.Id);
            if (actividadDb == null || actividad.Id==0)
            {
                _ctx.Actividades.Add(actividad);
            }
            else
            {
                actividadDb.Nombre = actividad.Nombre;
                actividadDb.Descripcion = actividad.Descripcion;
                _ctx.Entry(actividadDb).State = EntityState.Modified;
            }
            await _ctx.SaveChangesAsync();
            return actividad;
        }

        /// <summary>
        /// Elimina una actividad
        /// </summary>
        /// <param name="actividad">Actividad a eliminar</param>
        public async void Delete(Actividad actividad)
        {
            _ctx.Actividades.Remove(actividad);
            await _ctx.SaveChangesAsync();
        }



    }
}
