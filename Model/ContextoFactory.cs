﻿using ControlMetadata.ViewModel.Helpers;
using System;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static ControlMetadata.Enumerador;

namespace ControlMetadata.Model
{
    public class ContextoFactory : IDbContextFactory<Contexto>
    {
        public Contexto Create()
        {
            //var ctx = Contexto.GetMetadataInstance();
            var ctx = new Contexto(HelperConnectionDb.GetConnectionString(ConnectionStringFile.Metadata));//"xew");
            return ctx;
        }
    }
}
