﻿using ControlMetadata.Model.Entities;
using ControlMetadata.ViewModel.Helpers;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static ControlMetadata.Enumerador;

namespace ControlMetadata.Model
{
    /// <summary>
    /// Contexto de datos
    /// </summary>
    public class Contexto:DbContext
    {
       
        private static Contexto metadataContext=null; 

        public Contexto(string connectionString): base(connectionString)
        {
        }

        /// <summary>
        /// Singleton de la clase
        /// </summary>
        /// <returns></returns>
        public static Contexto GetInstance()
        {
            if (metadataContext == null)
            {
                metadataContext= new Contexto(HelperConnectionDb.GetConnectionString(ConnectionStringFile.Metadata));
            }
            return metadataContext;
        }

        /// <summary>
        /// OnModelCreating
        /// </summary>
        /// <param name="modelBuilder"></param>
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<Metadata>().HasIndex(m => new { m.ObjetoId, m.Numero }).IsUnique();

            modelBuilder.Entity<Usuario>().HasIndex(u => u.Username).IsUnique(true);
        }

        /// <summary>
        /// Actividades
        /// </summary>
        public DbSet<Actividad> Actividades { get; set; }

        /// <summary>
        /// Metadata
        /// </summary>
        public DbSet<Metadata> Metadata { get; set; }

        /// <summary>
        /// Usuarios
        /// </summary>
        public DbSet<Usuario> Usuarios { get; set; }

    }
}
