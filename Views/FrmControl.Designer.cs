﻿
namespace ControlMetadata
{
    partial class FrmControl
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.tabPrincipal = new System.Windows.Forms.TabControl();
            this.tpPage2 = new System.Windows.Forms.TabPage();
            this.tab2gvDetalle = new System.Windows.Forms.DataGridView();
            this.tab2gcColNombre = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tab2gcColDescripcion = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.gbGeneralActividades = new System.Windows.Forms.GroupBox();
            this.tab2BtnGuardar = new System.Windows.Forms.Button();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.tab2TxtDescripcion = new System.Windows.Forms.TextBox();
            this.tab2TxtNombre = new System.Windows.Forms.TextBox();
            this.tpPage1 = new System.Windows.Forms.TabPage();
            this.tab1gvDetalle = new System.Windows.Forms.DataGridView();
            this.TipoObjeto = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Numero = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Descripcion = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tab1GbGeneral = new System.Windows.Forms.GroupBox();
            this.tab2BtnEliminar = new System.Windows.Forms.Button();
            this.tab1BtnGuardar = new System.Windows.Forms.Button();
            this.tab1TxtDescripcion = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.tab1CmbActividad = new System.Windows.Forms.ComboBox();
            this.tab1CmbTipoObjeto = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.tab1TxtNumero = new System.Windows.Forms.TextBox();
            this.tpPage3 = new System.Windows.Forms.TabPage();
            this.label3 = new System.Windows.Forms.Label();
            this.tab3BtnScriptGenerar = new System.Windows.Forms.Button();
            this.tab3CmbScriptActividad = new System.Windows.Forms.ComboBox();
            this.tab3RtbScript = new System.Windows.Forms.RichTextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.tabPrincipal.SuspendLayout();
            this.tpPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tab2gvDetalle)).BeginInit();
            this.gbGeneralActividades.SuspendLayout();
            this.tpPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tab1gvDetalle)).BeginInit();
            this.tab1GbGeneral.SuspendLayout();
            this.tpPage3.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabPrincipal
            // 
            this.tabPrincipal.Controls.Add(this.tpPage2);
            this.tabPrincipal.Controls.Add(this.tpPage1);
            this.tabPrincipal.Controls.Add(this.tpPage3);
            this.tabPrincipal.Location = new System.Drawing.Point(7, 51);
            this.tabPrincipal.Name = "tabPrincipal";
            this.tabPrincipal.SelectedIndex = 0;
            this.tabPrincipal.Size = new System.Drawing.Size(634, 464);
            this.tabPrincipal.TabIndex = 0;
            // 
            // tpPage2
            // 
            this.tpPage2.Controls.Add(this.tab2gvDetalle);
            this.tpPage2.Controls.Add(this.gbGeneralActividades);
            this.tpPage2.Location = new System.Drawing.Point(4, 25);
            this.tpPage2.Name = "tpPage2";
            this.tpPage2.Size = new System.Drawing.Size(626, 435);
            this.tpPage2.TabIndex = 2;
            this.tpPage2.Text = "Actividades";
            this.tpPage2.UseVisualStyleBackColor = true;
            // 
            // tab2gvDetalle
            // 
            this.tab2gvDetalle.AllowUserToAddRows = false;
            this.tab2gvDetalle.AllowUserToDeleteRows = false;
            this.tab2gvDetalle.BackgroundColor = System.Drawing.Color.White;
            this.tab2gvDetalle.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.tab2gvDetalle.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.tab2gcColNombre,
            this.tab2gcColDescripcion});
            this.tab2gvDetalle.Location = new System.Drawing.Point(10, 177);
            this.tab2gvDetalle.Name = "tab2gvDetalle";
            this.tab2gvDetalle.RowHeadersWidth = 51;
            this.tab2gvDetalle.RowTemplate.Height = 24;
            this.tab2gvDetalle.Size = new System.Drawing.Size(605, 251);
            this.tab2gvDetalle.TabIndex = 1;
            // 
            // tab2gcColNombre
            // 
            this.tab2gcColNombre.DataPropertyName = "Nombre";
            this.tab2gcColNombre.HeaderText = "Actividad";
            this.tab2gcColNombre.MaxInputLength = 150;
            this.tab2gcColNombre.MinimumWidth = 6;
            this.tab2gcColNombre.Name = "tab2gcColNombre";
            this.tab2gcColNombre.ReadOnly = true;
            this.tab2gcColNombre.Width = 150;
            // 
            // tab2gcColDescripcion
            // 
            this.tab2gcColDescripcion.DataPropertyName = "Descripcion";
            this.tab2gcColDescripcion.HeaderText = "Descripción";
            this.tab2gcColDescripcion.MaxInputLength = 300;
            this.tab2gcColDescripcion.MinimumWidth = 6;
            this.tab2gcColDescripcion.Name = "tab2gcColDescripcion";
            this.tab2gcColDescripcion.ReadOnly = true;
            this.tab2gcColDescripcion.Width = 300;
            // 
            // gbGeneralActividades
            // 
            this.gbGeneralActividades.Controls.Add(this.tab2BtnGuardar);
            this.gbGeneralActividades.Controls.Add(this.label7);
            this.gbGeneralActividades.Controls.Add(this.label6);
            this.gbGeneralActividades.Controls.Add(this.tab2TxtDescripcion);
            this.gbGeneralActividades.Controls.Add(this.tab2TxtNombre);
            this.gbGeneralActividades.Location = new System.Drawing.Point(10, 15);
            this.gbGeneralActividades.Name = "gbGeneralActividades";
            this.gbGeneralActividades.Size = new System.Drawing.Size(605, 156);
            this.gbGeneralActividades.TabIndex = 0;
            this.gbGeneralActividades.TabStop = false;
            this.gbGeneralActividades.Text = "Actividades:";
            // 
            // tab2BtnGuardar
            // 
            this.tab2BtnGuardar.Location = new System.Drawing.Point(489, 116);
            this.tab2BtnGuardar.Name = "tab2BtnGuardar";
            this.tab2BtnGuardar.Size = new System.Drawing.Size(110, 29);
            this.tab2BtnGuardar.TabIndex = 4;
            this.tab2BtnGuardar.Text = "Guardar";
            this.tab2BtnGuardar.UseVisualStyleBackColor = true;
            this.tab2BtnGuardar.Click += new System.EventHandler(this.tab2BtnGuardar_Click);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(10, 36);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(69, 17);
            this.label7.TabIndex = 3;
            this.label7.Text = "Actividad:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(7, 65);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(86, 17);
            this.label6.TabIndex = 2;
            this.label6.Text = "Descripción:";
            // 
            // tab2TxtDescripcion
            // 
            this.tab2TxtDescripcion.Location = new System.Drawing.Point(99, 65);
            this.tab2TxtDescripcion.MaxLength = 300;
            this.tab2TxtDescripcion.Multiline = true;
            this.tab2TxtDescripcion.Name = "tab2TxtDescripcion";
            this.tab2TxtDescripcion.Size = new System.Drawing.Size(500, 45);
            this.tab2TxtDescripcion.TabIndex = 1;
            // 
            // tab2TxtNombre
            // 
            this.tab2TxtNombre.Location = new System.Drawing.Point(99, 36);
            this.tab2TxtNombre.MaxLength = 150;
            this.tab2TxtNombre.Name = "tab2TxtNombre";
            this.tab2TxtNombre.Size = new System.Drawing.Size(500, 22);
            this.tab2TxtNombre.TabIndex = 0;
            // 
            // tpPage1
            // 
            this.tpPage1.Controls.Add(this.tab1gvDetalle);
            this.tpPage1.Controls.Add(this.tab1GbGeneral);
            this.tpPage1.Location = new System.Drawing.Point(4, 25);
            this.tpPage1.Name = "tpPage1";
            this.tpPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tpPage1.Size = new System.Drawing.Size(626, 435);
            this.tpPage1.TabIndex = 0;
            this.tpPage1.Text = "Registrar";
            this.tpPage1.UseVisualStyleBackColor = true;
            // 
            // tab1gvDetalle
            // 
            this.tab1gvDetalle.AllowUserToAddRows = false;
            this.tab1gvDetalle.AllowUserToDeleteRows = false;
            this.tab1gvDetalle.BackgroundColor = System.Drawing.Color.White;
            this.tab1gvDetalle.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.tab1gvDetalle.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.TipoObjeto,
            this.Numero,
            this.Descripcion});
            this.tab1gvDetalle.Location = new System.Drawing.Point(6, 189);
            this.tab1gvDetalle.Name = "tab1gvDetalle";
            this.tab1gvDetalle.RowHeadersWidth = 51;
            this.tab1gvDetalle.RowTemplate.Height = 24;
            this.tab1gvDetalle.Size = new System.Drawing.Size(614, 240);
            this.tab1gvDetalle.TabIndex = 9;
            // 
            // TipoObjeto
            // 
            this.TipoObjeto.DataPropertyName = "ObjetoId";
            this.TipoObjeto.HeaderText = "Tipo Objeto";
            this.TipoObjeto.MaxInputLength = 2;
            this.TipoObjeto.MinimumWidth = 6;
            this.TipoObjeto.Name = "TipoObjeto";
            this.TipoObjeto.ReadOnly = true;
            this.TipoObjeto.Width = 120;
            // 
            // Numero
            // 
            this.Numero.DataPropertyName = "Numero";
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.Numero.DefaultCellStyle = dataGridViewCellStyle2;
            this.Numero.HeaderText = "Número";
            this.Numero.MaxInputLength = 8;
            this.Numero.MinimumWidth = 6;
            this.Numero.Name = "Numero";
            this.Numero.ReadOnly = true;
            this.Numero.Width = 60;
            // 
            // Descripcion
            // 
            this.Descripcion.DataPropertyName = "Descripcion";
            this.Descripcion.HeaderText = "Descripción";
            this.Descripcion.MaxInputLength = 150;
            this.Descripcion.MinimumWidth = 6;
            this.Descripcion.Name = "Descripcion";
            this.Descripcion.ReadOnly = true;
            this.Descripcion.Width = 300;
            // 
            // tab1GbGeneral
            // 
            this.tab1GbGeneral.Controls.Add(this.tab2BtnEliminar);
            this.tab1GbGeneral.Controls.Add(this.tab1BtnGuardar);
            this.tab1GbGeneral.Controls.Add(this.tab1TxtDescripcion);
            this.tab1GbGeneral.Controls.Add(this.label4);
            this.tab1GbGeneral.Controls.Add(this.label5);
            this.tab1GbGeneral.Controls.Add(this.tab1CmbActividad);
            this.tab1GbGeneral.Controls.Add(this.tab1CmbTipoObjeto);
            this.tab1GbGeneral.Controls.Add(this.label2);
            this.tab1GbGeneral.Controls.Add(this.label1);
            this.tab1GbGeneral.Controls.Add(this.tab1TxtNumero);
            this.tab1GbGeneral.Location = new System.Drawing.Point(6, 17);
            this.tab1GbGeneral.Name = "tab1GbGeneral";
            this.tab1GbGeneral.Size = new System.Drawing.Size(614, 160);
            this.tab1GbGeneral.TabIndex = 8;
            this.tab1GbGeneral.TabStop = false;
            this.tab1GbGeneral.Text = "Registrar metadata:";
            // 
            // tab2BtnEliminar
            // 
            this.tab2BtnEliminar.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tab2BtnEliminar.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.tab2BtnEliminar.Location = new System.Drawing.Point(133, 120);
            this.tab2BtnEliminar.Name = "tab2BtnEliminar";
            this.tab2BtnEliminar.Size = new System.Drawing.Size(110, 29);
            this.tab2BtnEliminar.TabIndex = 9;
            this.tab2BtnEliminar.Text = "&Eliminar";
            this.tab2BtnEliminar.UseVisualStyleBackColor = true;
            this.tab2BtnEliminar.Click += new System.EventHandler(this.tab2BtnEliminar_Click);
            // 
            // tab1BtnGuardar
            // 
            this.tab1BtnGuardar.Location = new System.Drawing.Point(498, 120);
            this.tab1BtnGuardar.Name = "tab1BtnGuardar";
            this.tab1BtnGuardar.Size = new System.Drawing.Size(110, 31);
            this.tab1BtnGuardar.TabIndex = 8;
            this.tab1BtnGuardar.Text = "&Guardar";
            this.tab1BtnGuardar.UseVisualStyleBackColor = true;
            this.tab1BtnGuardar.Click += new System.EventHandler(this.tab1BtnGuardar_Click);
            // 
            // tab1TxtDescripcion
            // 
            this.tab1TxtDescripcion.Location = new System.Drawing.Point(133, 92);
            this.tab1TxtDescripcion.MaxLength = 150;
            this.tab1TxtDescripcion.Name = "tab1TxtDescripcion";
            this.tab1TxtDescripcion.Size = new System.Drawing.Size(475, 22);
            this.tab1TxtDescripcion.TabIndex = 7;
            this.tab1TxtDescripcion.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tab1TxtDescripcion_KeyPress);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(7, 95);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(86, 17);
            this.label4.TabIndex = 7;
            this.label4.Text = "Descripción:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(7, 34);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(69, 17);
            this.label5.TabIndex = 1;
            this.label5.Text = "Actividad:";
            // 
            // tab1CmbActividad
            // 
            this.tab1CmbActividad.DisplayMember = "Nombre";
            this.tab1CmbActividad.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.tab1CmbActividad.FormattingEnabled = true;
            this.tab1CmbActividad.Location = new System.Drawing.Point(133, 31);
            this.tab1CmbActividad.Name = "tab1CmbActividad";
            this.tab1CmbActividad.Size = new System.Drawing.Size(475, 24);
            this.tab1CmbActividad.TabIndex = 0;
            this.tab1CmbActividad.ValueMember = "Id";
            this.tab1CmbActividad.SelectedIndexChanged += new System.EventHandler(this.tab1CmbActividad_SelectedIndexChanged);
            // 
            // tab1CmbTipoObjeto
            // 
            this.tab1CmbTipoObjeto.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.tab1CmbTipoObjeto.FormattingEnabled = true;
            this.tab1CmbTipoObjeto.Location = new System.Drawing.Point(133, 61);
            this.tab1CmbTipoObjeto.Name = "tab1CmbTipoObjeto";
            this.tab1CmbTipoObjeto.Size = new System.Drawing.Size(213, 24);
            this.tab1CmbTipoObjeto.TabIndex = 0;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(420, 64);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(62, 17);
            this.label2.TabIndex = 5;
            this.label2.Text = "Numero:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(7, 64);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(86, 17);
            this.label1.TabIndex = 4;
            this.label1.Text = "Tipo Objeto:";
            // 
            // tab1TxtNumero
            // 
            this.tab1TxtNumero.Location = new System.Drawing.Point(498, 64);
            this.tab1TxtNumero.MaxLength = 8;
            this.tab1TxtNumero.Name = "tab1TxtNumero";
            this.tab1TxtNumero.Size = new System.Drawing.Size(110, 22);
            this.tab1TxtNumero.TabIndex = 1;
            this.tab1TxtNumero.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.tab1TxtNumero.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tab1TxtNumero_KeyPress);
            // 
            // tpPage3
            // 
            this.tpPage3.Controls.Add(this.label3);
            this.tpPage3.Controls.Add(this.tab3BtnScriptGenerar);
            this.tpPage3.Controls.Add(this.tab3CmbScriptActividad);
            this.tpPage3.Controls.Add(this.tab3RtbScript);
            this.tpPage3.Location = new System.Drawing.Point(4, 25);
            this.tpPage3.Name = "tpPage3";
            this.tpPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tpPage3.Size = new System.Drawing.Size(626, 435);
            this.tpPage3.TabIndex = 1;
            this.tpPage3.Text = "Generar Script";
            this.tpPage3.UseVisualStyleBackColor = true;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(15, 19);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(69, 17);
            this.label3.TabIndex = 3;
            this.label3.Text = "Actividad:";
            // 
            // tab3BtnScriptGenerar
            // 
            this.tab3BtnScriptGenerar.Location = new System.Drawing.Point(417, 12);
            this.tab3BtnScriptGenerar.Name = "tab3BtnScriptGenerar";
            this.tab3BtnScriptGenerar.Size = new System.Drawing.Size(104, 28);
            this.tab3BtnScriptGenerar.TabIndex = 2;
            this.tab3BtnScriptGenerar.Text = "Generar Script";
            this.tab3BtnScriptGenerar.UseVisualStyleBackColor = true;
            this.tab3BtnScriptGenerar.Click += new System.EventHandler(this.tab3BtnScriptGenerar_Click);
            // 
            // tab3CmbScriptActividad
            // 
            this.tab3CmbScriptActividad.DisplayMember = "Nombre";
            this.tab3CmbScriptActividad.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.tab3CmbScriptActividad.FormattingEnabled = true;
            this.tab3CmbScriptActividad.Location = new System.Drawing.Point(99, 16);
            this.tab3CmbScriptActividad.Name = "tab3CmbScriptActividad";
            this.tab3CmbScriptActividad.Size = new System.Drawing.Size(302, 24);
            this.tab3CmbScriptActividad.TabIndex = 1;
            this.tab3CmbScriptActividad.ValueMember = "Id";
            // 
            // tab3RtbScript
            // 
            this.tab3RtbScript.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tab3RtbScript.Location = new System.Drawing.Point(6, 46);
            this.tab3RtbScript.Name = "tab3RtbScript";
            this.tab3RtbScript.ReadOnly = true;
            this.tab3RtbScript.Size = new System.Drawing.Size(614, 386);
            this.tab3RtbScript.TabIndex = 0;
            this.tab3RtbScript.Text = "";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Segoe Script", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.White;
            this.label8.Location = new System.Drawing.Point(162, 9);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(347, 40);
            this.label8.TabIndex = 1;
            this.label8.Text = "METADATA GENERATOR";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Segoe Script", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.label9.Location = new System.Drawing.Point(273, 518);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(104, 28);
            this.label9.TabIndex = 2;
            this.label9.Text = "By Kruger";
            // 
            // FrmControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Teal;
            this.ClientSize = new System.Drawing.Size(647, 549);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.tabPrincipal);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "FrmControl";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Control de Metadata";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.FrmControl_FormClosed);
            this.Load += new System.EventHandler(this.Form1_Load);
            this.tabPrincipal.ResumeLayout(false);
            this.tpPage2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.tab2gvDetalle)).EndInit();
            this.gbGeneralActividades.ResumeLayout(false);
            this.gbGeneralActividades.PerformLayout();
            this.tpPage1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.tab1gvDetalle)).EndInit();
            this.tab1GbGeneral.ResumeLayout(false);
            this.tab1GbGeneral.PerformLayout();
            this.tpPage3.ResumeLayout(false);
            this.tpPage3.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TabControl tabPrincipal;
        private System.Windows.Forms.TabPage tpPage1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox tab1TxtNumero;
        private System.Windows.Forms.ComboBox tab1CmbTipoObjeto;
        private System.Windows.Forms.TabPage tpPage3;
        private System.Windows.Forms.GroupBox tab1GbGeneral;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ComboBox tab1CmbActividad;
        private System.Windows.Forms.DataGridView tab1gvDetalle;
        private System.Windows.Forms.TextBox tab1TxtDescripcion;
        private System.Windows.Forms.RichTextBox tab3RtbScript;
        private System.Windows.Forms.Button tab3BtnScriptGenerar;
        private System.Windows.Forms.ComboBox tab3CmbScriptActividad;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button tab1BtnGuardar;
        private System.Windows.Forms.TabPage tpPage2;
        private System.Windows.Forms.DataGridView tab2gvDetalle;
        private System.Windows.Forms.GroupBox gbGeneralActividades;
        private System.Windows.Forms.Button tab2BtnGuardar;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox tab2TxtDescripcion;
        private System.Windows.Forms.TextBox tab2TxtNombre;
        private System.Windows.Forms.DataGridViewTextBoxColumn tab2gcColNombre;
        private System.Windows.Forms.DataGridViewTextBoxColumn tab2gcColDescripcion;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.DataGridViewTextBoxColumn TipoObjeto;
        private System.Windows.Forms.DataGridViewTextBoxColumn Numero;
        private System.Windows.Forms.DataGridViewTextBoxColumn Descripcion;
        private System.Windows.Forms.Button tab2BtnEliminar;
    }
}

