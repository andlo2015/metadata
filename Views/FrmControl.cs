﻿using ControlMetadata.Model;
using ControlMetadata.Model.Entities;
using ControlMetadata.Model.Repositories;
using ControlMetadata.Topaz;
using ControlMetadata.Topaz.Interfaces;
using ControlMetadata.ViewModel;
using ControlMetadata.ViewModel.Clases;
using ControlMetadata.ViewModel.Helpers;
using PetaPoco;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using static ControlMetadata.Enumerador;

namespace ControlMetadata
{
    public partial class FrmControl : Form
    {

        private Contexto _ctx;
        private Database _topazDb;
        private MetadataViewModel _vmMetadata;
        private ActividadViewModel _vmActividad;
        private int _usuarioId=0;


        public FrmControl()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            _usuarioId = Constantes.User?.Id ?? 0;
            _ctx = Contexto.GetInstance();
            _topazDb = TopazDb.GetInstance();
            _vmMetadata = new MetadataViewModel(_ctx);
            _vmActividad = new ActividadViewModel(_ctx);
            tabPrincipal.SelectedTab =tpPage1;
            _Inicializa();
        }

        /// <summary>
        /// Inicializa los datos
        /// </summary>
        private void _Inicializa()
        {
           
            tab1CmbTipoObjeto.DataSource = Enum.GetValues(typeof(ObjetosTopazCombo));
            CargarActividades();
        }

        /// <summary>
        /// Carga las actividades en los combos y en la grilla de actividades
        /// </summary>
        private void CargarActividades()
        {
            var actividades = _vmActividad.GetByUserId(_usuarioId);
            tab1CmbActividad.DataSource = actividades;
            tab2gvDetalle.AutoGenerateColumns = false;
            tab2gvDetalle.DataSource = actividades;
            tab3CmbScriptActividad.DataSource = actividades;
        }


       
        //METODOS DEL FORMULARIO
        #region METODOS

        #region TAB_1-METADATA

        /// <summary>
        /// Carga el detalle de metadata
        /// </summary>
        private void Tab1MetadataGetDetalle()
        {
            try
            {
                int.TryParse(tab1CmbActividad.SelectedValue.ToString(), out int actividadId);
                tab1gvDetalle.AutoGenerateColumns = false;
                var detalle = _vmMetadata.GetByActividadId(actividadId)
                        .OrderBy(m => m.ObjetoId).ThenBy(m => m.Numero).ToList();
                tab1gvDetalle.DataSource = detalle;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        /// <summary>
        /// Elimina un registro de Metadata
        /// </summary>
        private void Tab1MetadataEliminar()
        {
            var row = tab1gvDetalle.CurrentRow?.DataBoundItem as Metadata;
            if (row != null)
            {
                _vmMetadata.Delete(row);
                Tab1MetadataGetDetalle();
            }
        }

        /// <summary>
        /// Guarda la metadata
        /// </summary>
        private async void Tab1MetadataGuardar()
        {
            try
            {
                if (Tab1MetadataValidar())
                {
                    int.TryParse(tab1CmbActividad.SelectedValue.ToString(), out int actividadId);
                    Metadata registro = new Metadata()
                    {
                        ActividadId = actividadId,
                        Descripcion = tab1TxtDescripcion.Text.Trim(),
                        Estado = true,
                        Id = 0,
                        Numero = Convert.ToInt32(tab1TxtNumero.Text),
                        ObjetoId = (ObjetosTopaz)tab1CmbTipoObjeto.SelectedValue
                    };

                    await _vmMetadata.Save(registro);
                    Tab1MetadataGetDetalle();
                    tab1TxtDescripcion.Text = tab1TxtNumero.Text = "";
                    tab1TxtNumero.Focus();
                }
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message, "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        /// <summary>
        /// Valida la información de la metadata
        /// </summary>
        /// <returns></returns>
        private bool Tab1MetadataValidar()
        {
            if (tab1CmbActividad.SelectedValue == null)
            {
                MessageBox.Show("Por favor seleccione una actividad", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                tab1CmbActividad.Focus();
                return false;
            }
            if (tab1CmbTipoObjeto.SelectedValue == null)
            {
                MessageBox.Show("Por favor seleccione un tipo de objeto Topaz", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                tab1CmbTipoObjeto.Focus();
                return false;
            }
            if ((ObjetosTopaz)tab1CmbTipoObjeto.SelectedValue == ObjetosTopaz.OpcionCampo)
            {
                MessageBox.Show("La metadata para el tipo de objeto seleccionado. Se genera de forma automática, NO hace falta registrar", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                tab1CmbTipoObjeto.Focus();
                return false;
            }
            else if (tab1TxtDescripcion.Text.Trim() == "")
            {
                MessageBox.Show("Por favor ingrese una descripción", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                tab1TxtDescripcion.Focus();
                return false;
            }
            else if (tab1TxtNumero.Text.Trim() == "")
            {
                MessageBox.Show("Por favor ingrese el número de objeto", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                tab1TxtNumero.Focus();
                return false;
            }
            return true;
        }

        #endregion

        //METODOS PARA ACTIVIDAD
        #region TAB_2-ACTIVIDAD

        /// <summary>
        /// Guarda una actividad
        /// </summary>
        private async void Tab2ActividadGuardar()
        {
            try { 
            if (Tab2ActividadValidar())
            {
                Actividad actividad = new Actividad()
                {
                    Descripcion = tab2TxtDescripcion.Text.Trim(),
                    Estado = true,
                    Nombre = tab2TxtNombre.Text.Trim(),
                    UsuarioId = _usuarioId,
                    Id = 0
                };
                await _vmActividad.Save(actividad);
                CargarActividades();
            }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        /// <summary>
        /// Valida la informacion de actividad
        /// </summary>
        /// <returns></returns>
        private bool Tab2ActividadValidar()
        {
            if (tab2TxtNombre.Text.Trim() == "")
            {
                MessageBox.Show("Por favor ingrese un nombre para la actividad", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                tab2TxtNombre.Focus();
                return false;
            }
            else if (tab2TxtDescripcion.Text.Trim() == "")
            {
                MessageBox.Show("Por favor ingrese una descripción para la actividad", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                tab2TxtDescripcion.Focus();
                return false;
            }
            return true;
        }

        #endregion

        //METODOTOS PARA GENERAR EL SCRIPT
        #region TAB_3_SCRIPT

        /// <summary>
        /// Genera el script de Metadata
        /// </summary>
        private void Tab3ScriptGenerar()
        {
            try
            {
                int actividadId = 0;
                int.TryParse(tab3CmbScriptActividad.SelectedValue.ToString(), out actividadId);
                if (actividadId > 0)
                {

                    MetadataGenerator generator = new MetadataGenerator();

                    var metadata = _vmMetadata.GetByActividadId(actividadId).ToList();

                    string resultado = "";
                    List<ITopazMetadata> listaProcesar = new List<ITopazMetadata>()
                {
                    new DescriptorViewModel(_topazDb, generator),
                    //Campos de descriptores y campos libres
                    new CampoViewModel(_topazDb, generator),
                    new IndiceViewModel(_topazDb, generator),
                    //Ayudas y vistas
                    new VistaViewModel(_topazDb, generator),
                    new CampoOpcionViewModel(_topazDb, generator),
                    new OperacionViewModel(_topazDb, generator),
                    //Numeradores Tabla "NumeratorDefinition"
                    new NumeratorDefinitionViewModel(_topazDb,generator),
                    //Numeradores Tabla "NumeratorValues"
                    new NumeratorValueViewModel(_topazDb,generator)
                };

                    listaProcesar.ForEach(row =>
                    {
                        resultado += row.Generate(metadata) + "\n";
                    });

                    tab3RtbScript.Text = resultado;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        #endregion

        #endregion

        /// CONTROLES DEL FORMULARIO
        #region CONTROLES

        //CONTROLES METADATA
        #region TAB_1-METADATA

        private void tab2BtnEliminar_Click(object sender, EventArgs e)
        {
            Tab1MetadataEliminar();
        }

        private void tab1BtnGuardar_Click(object sender, EventArgs e)
        {
            Tab1MetadataGuardar();
        }

        private void tab1CmbActividad_SelectedIndexChanged(object sender, EventArgs e)
        {
            Tab1MetadataGetDetalle();
        }

        private void tab1TxtDescripcion_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                Tab1MetadataGuardar();
        }

        private void tab1TxtNumero_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsDigit(e.KeyChar) && e.KeyChar != 8 && e.KeyChar != 13)
            {
                e.Handled = true;
            }
        }

        #endregion

        /// <summary>
        /// CONTROLES ACTIVDADES
        /// </summary>
        #region TAB_2-ACTIVIDAD

        private void tab2BtnGuardar_Click(object sender, EventArgs e)
        {
            Tab2ActividadGuardar();
        }

        #endregion

        /// <summary>
        /// CONTROLES PARA GENERAR SCRIPT DE METADATA
        /// </summary>
        #region TAB_3-SCRIPT

        private void tab3BtnScriptGenerar_Click(object sender, EventArgs e)
        {
            Tab3ScriptGenerar();
        }



        #endregion

        #endregion

        private void FrmControl_FormClosed(object sender, FormClosedEventArgs e)
        {
            Application.Exit();
        }
    }
}
