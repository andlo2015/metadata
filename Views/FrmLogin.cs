﻿using ControlMetadata.Model;
using ControlMetadata.ViewModel.MetadataVm;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ControlMetadata.Views
{
    public partial class FrmLogin : Form
    {

        private int _intentos = 0;
        public FrmLogin()
        {
            InitializeComponent();
        }

        private void FrmLogin_Load(object sender, EventArgs e)
        {

        }

        /// <summary>
        /// Ingresa el usuario
        /// </summary>
        private void Login()
        {
            if (txtUsername.Text.Trim() != "" && txtPassword.Text.Trim() != "")
            {
                if (_intentos < 3)
                {
                    UsuarioViewModel _vmUsuario = new UsuarioViewModel(Contexto.GetInstance());
                    var usuario = _vmUsuario.Login(txtUsername.Text.Trim(), txtPassword.Text.Trim());
                    if (usuario != null)
                    {
                        Constantes.User = usuario;
                        FrmControl frmControl = new FrmControl();
                        frmControl.Show();
                        this.Hide();
                    }
                    else
                    {
                        _intentos++;
                        MessageBox.Show("Usuario o clave incorrecto", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        txtUsername.Focus();
                    }
                }
                else
                {
                    MessageBox.Show("Ha excedido el número de intentos", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            else
            {
                MessageBox.Show("Por favor ingrese el usuario y contraseña", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }


        private void btnIngresar_Click(object sender, EventArgs e)
        {
            Login();
        }

        private void txtPassword_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                btnIngresar.Focus();
        }

    }
}
