﻿using ControlMetadata.Model.Entities;
using System.Collections.Generic;
using System.Linq;
using static ControlMetadata.Enumerador;

namespace ControlMetadata.ViewModel.Helpers
{
    /// <summary>
    /// Helper para acciones con la metadata
    /// </summary>
    public static class HelperMetadata
    {

        /// <summary>
        /// Filtra la metadata por un tipo de objeto específico
        /// </summary>
        /// <param name="metaData">Metadata en la que va a consultar</param>
        /// <returns></returns>
        public static List<Metadata> GetByTipo(this IEnumerable<Metadata> metaData,ObjetosTopaz tipoObjeto)
        {
            return metaData.Where(m => m.ObjetoId==tipoObjeto).ToList();
        }


        /// <summary>
        /// Devuelve los Numeros de objeto Topaz de un listado de metadata
        /// </summary>
        /// <param name="metaData">Metadata en la que va a consultar</param>
        /// <returns></returns>
        public static List<int> GetNumeroTopaz(this IEnumerable<Metadata> metaData)
        {
            return metaData.Select(m => m.Numero).ToList();
        }


    }
}
