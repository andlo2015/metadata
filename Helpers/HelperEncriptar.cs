﻿using System;
using System.Security.Cryptography;
using System.Text;

namespace ControlMetadata.ViewModel.Helpers
{
    /// <summary>
    /// Helper para encriptar
    /// </summary>
    public static class HelperEncriptar
    {

        /// <summary>
        /// Encripta un valor plano
        /// </summary>
        /// <param name="cadena">texto plano</param>
        /// <returns></returns>
        public static string Cifrar(this string cadena)
        {
            byte[] llave;
            byte[] arreglo = UTF8Encoding.UTF8.GetBytes(cadena);
            // Ciframos utilizando el Algoritmo MD5.
            MD5CryptoServiceProvider md5 = new MD5CryptoServiceProvider();
            llave = md5.ComputeHash(UTF8Encoding.UTF8.GetBytes(Constantes.Key));
            md5.Clear();
            //Ciframos utilizando el Algoritmo 3DES.
            TripleDESCryptoServiceProvider tripledes = new TripleDESCryptoServiceProvider();
            tripledes.Key = llave;
            tripledes.Mode = CipherMode.ECB;
            tripledes.Padding = PaddingMode.PKCS7;
            ICryptoTransform convertir = tripledes.CreateEncryptor();
            byte[] resultado = convertir.TransformFinalBlock(arreglo, 0, arreglo.Length);
            tripledes.Clear();
            return Convert.ToBase64String(resultado, 0, resultado.Length);
        }

        /// <summary>
        /// Descfira un valor encriptado
        /// </summary>
        /// <param name="cadena">texto de entrada</param>
        /// <returns></returns>
        public static string Descifrar(this string cadena)
        {
            byte[] llave;
            byte[] arreglo = Convert.FromBase64String(cadena);
            MD5CryptoServiceProvider md5 = new MD5CryptoServiceProvider();
            llave = md5.ComputeHash(UTF8Encoding.UTF8.GetBytes(Constantes.Key));
            md5.Clear();
            TripleDESCryptoServiceProvider tripledes = new TripleDESCryptoServiceProvider();
            tripledes.Key = llave;
            tripledes.Mode = CipherMode.ECB;
            tripledes.Padding = PaddingMode.PKCS7;
            ICryptoTransform convertir = tripledes.CreateDecryptor();
            byte[] resultado = convertir.TransformFinalBlock(arreglo, 0, arreglo.Length);
            tripledes.Clear();
            string cadena_descifrada = UTF8Encoding.UTF8.GetString(resultado);
            return cadena_descifrada;
        }

        /// <summary>
        /// Genera un SHA256 para cifrados para claves de usuario
        /// </summary>
        /// <param name="texto">texto a encriptar</param>
        /// <returns></returns>
        public static string GenerarSha256(this string texto)
        {
            // Create a SHA256   
            using (SHA256 sha256Hash = SHA256.Create())
            {
                // ComputeHash - returns byte array  
                byte[] bytes = sha256Hash.ComputeHash(Encoding.UTF8.GetBytes(texto));

                // Convert byte array to a string   
                StringBuilder builder = new StringBuilder();
                for (int i = 0; i < bytes.Length; i++)
                {
                    builder.Append(bytes[i].ToString("x2"));
                }
                return builder.ToString();
            }
        }

    }
}
