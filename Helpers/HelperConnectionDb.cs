﻿using ControlMetadata.ViewModel.Dtos;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static ControlMetadata.Enumerador;

namespace ControlMetadata.ViewModel.Helpers
{
    public static class HelperConnectionDb
    {
        /// <summary>
        /// Genera la cadena de conexion a las bases de datos
        /// </summary>
        /// <param name="opcion">Indica a que DB se va a conectar</param>
        /// <returns></returns>
        public static string GetConnectionString(ConnectionStringFile opcion)
        {
            string fileName = opcion == ConnectionStringFile.Metadata ? "metadata.json" : "topaz.json";
            string sqlConnection = "Data source=<ServerName>; Initial Catalog=<Database>;user id=<User>;password=<Password>";

            var infoConnection = JsonConvert.DeserializeObject<DtoConnection>(File.ReadAllText(fileName));
            if (infoConnection != null)
            {
                sqlConnection = sqlConnection.Replace("<ServerName>", infoConnection.ServerName);
                sqlConnection = sqlConnection.Replace("<Database>", infoConnection.Catalog);
                sqlConnection = sqlConnection.Replace("<User>", infoConnection.User.Descifrar());
                sqlConnection = sqlConnection.Replace("<Password>", infoConnection.Password.Descifrar());
            }
            return sqlConnection;
        }

    }
}
