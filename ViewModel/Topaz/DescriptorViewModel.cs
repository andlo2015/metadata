﻿using ControlMetadata.Model.Entities;
using ControlMetadata.Topaz.Dtos;
using ControlMetadata.Topaz.Interfaces;
using ControlMetadata.Topaz.Repositories;
using ControlMetadata.ViewModel.Interfaces;
using PetaPoco;
using System;
using System.Collections.Generic;
using static ControlMetadata.Enumerador;

namespace ControlMetadata.ViewModel
{
    /// <summary>
    /// Viewmodel 
    /// </summary>
    public class DescriptorViewModel:ITopazMetadata
    {
        private IMetadataGenerator _generator;
        private ITopazRepository _repository;

        public DescriptorViewModel(Database db, IMetadataGenerator generator)
        {
            _repository = new DescriptorRepository(db);
            _generator = generator;
        }

        /// <summary>
        /// Propiedad que contiene los registros para los cuales se va a generar la METADATA
        /// </summary>
        public List<object> Data { get; set; } = new List<object>();

        /// <summary>
        /// Tipo de objeto TOPAZ que va a procesar
        /// </summary>
        public ObjetosTopaz ObjectType { get; private set; } = ObjetosTopaz.Descriptor;


        /// <summary>
        /// Nombre de la tabla en la DB Topaz
        /// </summary>
        public string TableName { get; set; } = "Descriptores";

        /// <summary>
        /// Tipo de objeto con el que va a trabajar
        /// </summary>
        public Type Tipo { get; private set; } = typeof(DtoDescriptor);


        /// <summary>
        /// Genera la metadata del objeto
        /// </summary>
        /// <param name="metadata">Listado de objetos a procesar</param>
        public string Generate(List<Metadata> metadata)
        {
            GetData(metadata);
            return _generator.GenerateSql(this);
        }

        /// <summary>
        /// Recupera la metadata para la que se va a generar el SCRIPT
        /// </summary>
        /// <param name="metadata">Registros de metadata</param>
        private void GetData(List<Metadata> metadata)
        {
            //Limpia la variable
            Data = new List<object>();
            Data = _repository.GetData(metadata);
        }

        
    }
}
