﻿using ControlMetadata.Model;
using ControlMetadata.Model.Entities;
using ControlMetadata.Model.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ControlMetadata.ViewModel
{
    /// <summary>
    /// Viewmodel para la metadata
    /// </summary>
    public class MetadataViewModel
    {

        private MetadataRepository _repository;

        public MetadataViewModel(Contexto ctx)
        {
            _repository = new MetadataRepository(ctx);
        }

        /// <summary>
        /// Elimina un registro de metadata
        /// </summary>
        /// <param name="metadata"></param>
        public void Delete(Metadata metadata)
        {
            _repository.Delete(metadata);
        }

        /// <summary>
        /// Lista todos los registros que pertenecen a una actividad
        /// </summary>
        /// <param name="actividadId">Id de la actividad</param>
        /// <returns></returns>
        public List<Metadata> GetByActividadId(int actividadId)
        {
            return _repository.GetByActividadId(actividadId).ToList();
        }

        /// <summary>
        /// Guarda un registro de metadata
        /// </summary>
        /// <param name="metadata">Objeto a guardar</param>
        /// <returns></returns>
        public async Task<Metadata> Save(Metadata metadata)
        {
            return await _repository.Save(metadata);
        }

    }
}
