﻿using ControlMetadata.Model;
using ControlMetadata.Model.Entities;
using ControlMetadata.Model.Repositories;
using ControlMetadata.ViewModel.Helpers;

namespace ControlMetadata.ViewModel.MetadataVm
{
    /// <summary>
    /// Viewmodel usuario
    /// </summary>
    public class UsuarioViewModel
    {

        private UsuarioRepository _repository;
        public UsuarioViewModel(Contexto ctx)
        {
            _repository = new UsuarioRepository(ctx);
        }

        /// <summary>
        /// Autentica un usuario
        /// </summary>
        /// <param name="username">username</param>
        /// <param name="password">password</param>
        /// <returns></returns>
        public Usuario Login(string username,string password)
        {
            password = username + "-" + password;
            password = password.GenerarSha256();
            return _repository.Login(username,password);
        }

    }
}
