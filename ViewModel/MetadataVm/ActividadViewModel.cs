﻿using ControlMetadata.Model;
using ControlMetadata.Model.Entities;
using ControlMetadata.Model.Repositories;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ControlMetadata.ViewModel
{
    /// <summary>
    /// Viewmodel de actividad
    /// </summary>
    public class ActividadViewModel
    {
        private ActividadRepository _repository;

        public ActividadViewModel(Contexto ctx)
        {
            _repository = new ActividadRepository(ctx);
        }

        /// <summary>
        /// Lista las actividades de un usuario
        /// </summary>
        /// <param name="userId">usuario id</param>
        /// <returns></returns>
        public List<Actividad> GetByUserId(int userId)
        {
            return _repository.GetByUsuarioId(userId).ToList();
        }

        /// <summary>
        /// Guarda las actividades
        /// </summary>
        /// <param name="actividad">Actividad a guardar</param>
        /// <returns></returns>
        public async Task<Actividad> Save(Actividad actividad)
        {
            return await _repository.Save(actividad);
        }

    }
}
