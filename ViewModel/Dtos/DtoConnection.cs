﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ControlMetadata.ViewModel.Dtos
{
    /// <summary>
    /// Información de conexion
    /// </summary>
    public class DtoConnection
    {
        /// <summary>
        /// Nombre del servidor
        /// </summary>
        public string ServerName { get; set; } = "";

        /// <summary>
        /// Nombre de la base de datos
        /// </summary>
        public string Catalog { get; set; } = "";

        /// <summary>
        /// Nombre del usuario
        /// </summary>
        public string User { get; set; } = "";

        /// <summary>
        /// Password 
        /// </summary>
        public string Password { get; set; } = "";


    }
}
