﻿using ControlMetadata.Topaz.Interfaces;

namespace ControlMetadata.ViewModel.Interfaces
{
    public interface IMetadataGenerator
    {

        /// <summary>
        /// Genera las sentencias SQL 
        /// </summary>
        /// <param name="metadata">Objeto con los registros que van generar la metadata</param>
        string GenerateSql(ITopazMetadata metadata);

    }
}
