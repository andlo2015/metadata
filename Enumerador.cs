﻿using System.ComponentModel;

namespace ControlMetadata
{
    /// <summary>
    /// Enumeradores del proyecto
    /// </summary>
    public class Enumerador
    {

        /// <summary>
        /// Enumerador de los objetos de Topaz
        /// </summary>
        public enum ObjetosTopaz
        {
            [Description("Descriptor")]
            Descriptor=1,
            [Description("Campo Libre")]
            CampoLibre =2,
            [Description("Vista")]
            Vista =3,
            [Description("Operativa")]
            Operativa =4,
            [Description("Indice")]
            Indice =5,
            [Description("Numerador")]
            Numerador =6,
            [Description("Opciones de campo")]
            OpcionCampo =7
        }

        /// <summary>
        /// Debe coincidir la numeracion con ObjetosTopaz.
        /// Se utiliza para mostrar en los combos solo los tipos de objetos que debe ingresar
        /// Luego se hace un cast a ObjetosTopaz. (Es indispensable que los objetos comunes tengan el mismo numero)
        /// </summary>
        public enum ObjetosTopazCombo
        {
            [Description("Descriptor")]
            Descriptor = 1,
            [Description("Campo Libre")]
            CampoLibre = 2,
            [Description("Vista")]
            Vista = 3,
            [Description("Operativa")]
            Operativa = 4,
        }

        /// <summary>
        /// Cadenas de conexion a la base de datos
        /// </summary>
        public enum ConnectionStringFile
        {
            Metadata=1,
            TopazDb=2
        }

    }
}
