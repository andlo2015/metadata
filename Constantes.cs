﻿using ControlMetadata.Model.Entities;
using System;

namespace ControlMetadata
{
    /// <summary>
    /// Constantes del programa
    /// </summary>
    public class Constantes
    {
        /// <summary>
        /// El numero de concepto para los numeradores en TOPAZ
        /// </summary>
        public static int ConceptoNumerador = 19;

        /// <summary>
        /// Entero por defecto
        /// </summary>
        public static int DefaultInt { get { return -3; } }

        /// <summary>
        /// Fecha por defecto
        /// </summary>
        public static DateTime DefaultDateTime { get { return Convert.ToDateTime("2100-12-31T23:59:59"); } }

        /// <summary>
        /// Clave de cifrado en dos sentidos
        /// </summary>
        public static string Key { get { return "@kruger.fdlm*"; } }

        /// <summary>
        /// Usuario logueado
        /// </summary>
        public static Usuario User { get; set; }

    }
}
