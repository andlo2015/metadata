﻿using ControlMetadata.Topaz.Interfaces;
using ControlMetadata.ViewModel.Interfaces;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;

namespace ControlMetadata.ViewModel.Clases
{
    /// <summary>
    /// Genera las sentencias SQL
    /// </summary>
    public class MetadataGenerator:IMetadataGenerator
    {

        /// <summary>
        /// Contruye las sentencias SQL de los registros procesados
        /// </summary>
        /// <param name="metadata">Metadata a procesar</param>
        public string GenerateSql(ITopazMetadata metadata)
        {
            List<PropertyInfo> propiedades = metadata.Tipo.GetProperties().ToList();
            string script = $"--=====>{metadata.TableName.ToUpper()}\n\n";
            string tableName = metadata.TableName;
            //CABECERA DEL INSERT
            string Insert = $"GO\nINSERT INTO {tableName} (" + string.Join(",", propiedades.Select(p => p.Name)) + ")";

            foreach (var row in metadata.Data)
            {
                script += Insert + "\nVALUES (";
                string valoresColumna = "";
                foreach (var column in propiedades)
                {
                    var valorActual = (column.GetValue(row) ?? "NULL");
                    valoresColumna += valoresColumna != "" ? "," : "";
                    //Cuando es string o datetime. Agrega la comilla simple
                    valoresColumna += column.PropertyType == typeof(string) || column.PropertyType == typeof(DateTime) 
                                                            ? "'" + valorActual + "'" : valorActual;

                }
                script += valoresColumna + ")\n";

            }
            script = script.Replace("'NULL'", "NULL");
            script = script.Replace(Constantes.DefaultInt.ToString(), "NULL");
            return script;
        }

        


    }
}
