﻿using ControlMetadata.ViewModel.Helpers;
using PetaPoco;
using static ControlMetadata.Enumerador;

namespace ControlMetadata.Topaz
{
    /// <summary>
    /// Manejador de datos
    /// </summary>
    public class TopazDb
    {
        private static Database db=null;
        
        /// <summary>
        /// Singleton para base de datos de Topaz
        /// </summary>
        /// <returns></returns>
        public static Database GetInstance()
        {
            if (db == null)
            {
                db = new Database(HelperConnectionDb.GetConnectionString(ConnectionStringFile.TopazDb), "System.Data.SqlClient");
            }
            return db;
        }


    }
}
