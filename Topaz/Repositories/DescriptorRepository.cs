﻿using ControlMetadata.Model.Entities;
using ControlMetadata.Topaz.Dtos;
using ControlMetadata.Topaz.Interfaces;
using ControlMetadata.ViewModel.Helpers;
using PetaPoco;
using System.Collections.Generic;
using System.Linq;
using static ControlMetadata.Enumerador;

namespace ControlMetadata.Topaz.Repositories
{
    /// <summary>
    /// Reporitorio para descriptores
    /// </summary>
    public class DescriptorRepository:ITopazRepository
    {
        private Database _db;
         
        /// <summary>
        /// Constructor de la clase
        /// </summary>
        /// <param name="db"></param>
        public DescriptorRepository(Database db)
        {
            _db = db;
        }

        /// <summary>
        /// Recupera la informacion de la base de datos de Topaz
        /// </summary>
        /// <param name="metaData">Metadata</param>
        /// <returns></returns>
        public List<object> GetData(List<Metadata> metaData)
        {
            var listadoId = metaData.GetByTipo(ObjetosTopaz.Descriptor).GetNumeroTopaz();
           
            var data = new List<object>();
       
            if(listadoId?.Count>0)
                data.AddRange(_db.Query<DtoDescriptor>("SELECT * FROM DESCRIPTORES WHERE IDENTIFICACION IN (@0)", listadoId));
                   
            return data;
        }


    }
}
