﻿using ControlMetadata.Model.Entities;
using ControlMetadata.Topaz.Dtos;
using ControlMetadata.Topaz.Interfaces;
using ControlMetadata.ViewModel.Helpers;
using PetaPoco;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static ControlMetadata.Enumerador;

namespace ControlMetadata.Topaz.Repositories
{
    public class CampoOpcionRepository : ITopazRepository
    {

        private Database _db;

        /// <summary>
        /// Constructor de la clase
        /// </summary>
        /// <param name="db"></param>
        public CampoOpcionRepository(Database db)
        {
            _db = db;
        }

        /// <summary>
        /// Metodo principal para recuperar los registros de TopazDB
        /// </summary>
        /// <param name="metaData">Registros en la base Metadata</param>
        /// <returns></returns>
        public List<object> GetData(List<Metadata> metaData)
        {
            CampoRepository campoRepository = new CampoRepository(_db);
            var listadoCampos=campoRepository.GetAllNumerosDeCampos(metaData);
            List<object> data = new List<object>();
            if (listadoCampos?.Count > 0)
            {
                var opciones = _db.Fetch<DtoOpcion>("SELECT * FROM OPCIONES WHERE NUMERODECAMPO IN (@0)", listadoCampos).ToList();
                data.AddRange(opciones);
            }
            return data;
        }

    }
}
