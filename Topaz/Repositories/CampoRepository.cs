﻿using ControlMetadata.Model.Entities;
using ControlMetadata.Topaz.Dtos;
using ControlMetadata.Topaz.Interfaces;
using ControlMetadata.ViewModel.Helpers;
using PetaPoco;
using System.Collections.Generic;
using System.Linq;
using static ControlMetadata.Enumerador;

namespace ControlMetadata.Topaz.Repositories
{

    /// <summary>
    /// Reporitorio para descriptores
    /// </summary>
    public class CampoRepository:ITopazRepository
    {

        private Database _db;

        /// <summary>
        /// Constructor de la clase
        /// </summary>
        /// <param name="db"></param>
        public CampoRepository(Database db)
        {
            _db = db;
        }

        /// <summary>
        /// Recupera todos los campos de una actividad
        /// Campos en Descriptores y Campos Libres
        /// </summary>
        /// <param name="metaData">Metadata con la que está trabajando</param>
        /// <returns></returns>
        public List<DtoCampo> GetAllCampos(List<Metadata> metaData)
        {
            //Carga todos los campos de los descriptores
            List<int> listaDescriptores = metaData.GetByTipo(ObjetosTopaz.Descriptor).GetNumeroTopaz();
            //Carga todos los campos libres
            List<int> listaCamposLibres = metaData.GetByTipo(ObjetosTopaz.CampoLibre).GetNumeroTopaz();

            List<DtoCampo> campos = new List<DtoCampo>();
            
            if (listaDescriptores?.Count > 0)
                campos.AddRange(_db.Fetch<DtoCampo>("SELECT * FROM DICCIONARIO WHERE TABLA IN (@0)", listaDescriptores));
            
            if (listaCamposLibres?.Count > 0)
                campos.AddRange(_db.Fetch<DtoCampo>("SELECT * FROM Diccionario WHERE NumeroDeCampo IN (@0)", listaCamposLibres));
            
            return campos;
        }

        /// <summary>
        /// Devuelve los números de todos los campos de una actividad
        /// Campos en Descriptores y Campos Libres
        /// </summary>
        /// <param name="metaData">Metadata</param>
        /// <returns></returns>
        public List<int> GetAllNumerosDeCampos(List<Metadata> metaData)
        {
            var campos = GetAllCampos(metaData);
            return campos?.Select(c => c.NumeroDeCampo).ToList() ?? new List<int>();
        }

        /// <summary>
        /// Metodo principal para recuperar los registros de TopazDB
        /// </summary>
        /// <param name="metaData">Registros en la base Metadata</param>
        /// <returns></returns>
        public List<object> GetData(List<Metadata> metaData)
        {
            List<object> data = new List<object>();
            data.AddRange(GetAllCampos(metaData));
            return data;
        }

       

    }
}
