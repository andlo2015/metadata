﻿using ControlMetadata.Model.Entities;
using ControlMetadata.Topaz.Dtos;
using ControlMetadata.Topaz.Interfaces;
using PetaPoco;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ControlMetadata.Topaz.Repositories
{
    /// <summary>
    /// Recupera la información de la tabla NumeratorValues
    /// </summary>
    public class NumeratorValueRepository : ITopazRepository
    {
        private Database _db;

        /// <summary>
        /// Constructor de la clase
        /// </summary>
        /// <param name="db"></param>
        public NumeratorValueRepository(Database db)
        {
            _db = db;
        }

        /// <summary>
        /// Recupera la informacion de la base de datos de Topaz
        /// </summary>
        /// <param name="metaData">Metadata</param>
        /// <returns></returns>
        public List<object> GetData(List<Metadata> metaData)
        {
            CampoRepository campoRepository = new CampoRepository(_db);
            var listadoCampos = campoRepository.GetAllCampos(metaData);

            List<int> numeradoresId = listadoCampos.Where(c => c.Concepto == Constantes.ConceptoNumerador)
                            .Select(c => c.NumeroDeCampo).ToList();

            var data = new List<object>();

            if (numeradoresId?.Count > 0)
            {
                data.AddRange(_db.Query<DtoNumeratorValue>("SELECT * FROM NUMERATORVALUES WHERE NUMERO IN (@0)", numeradoresId));
            }
            return data;
        }

    }
}
