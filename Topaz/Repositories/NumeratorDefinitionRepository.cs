﻿using ControlMetadata.Model.Entities;
using ControlMetadata.Topaz.Dtos;
using ControlMetadata.Topaz.Interfaces;
using PetaPoco;
using System.Collections.Generic;
using System.Linq;

namespace ControlMetadata.Topaz.Repositories
{
    /// <summary>
    /// Repositorio para numeradores Topaz NumeratorDefinition
    /// </summary>
    public class NumeratorDefinitionRepository : ITopazRepository
    {
        private Database _db;

        /// <summary>
        /// Constructor de la clase
        /// </summary>
        /// <param name="db"></param>
        public NumeratorDefinitionRepository(Database db)
        {
            _db = db;
        }

        /// <summary>
        /// Recupera la informacion de la base de datos de Topaz
        /// </summary>
        /// <param name="metaData">Metadata</param>
        /// <returns></returns>
        public List<object> GetData(List<Metadata> metaData)
        {
            CampoRepository campoRepository = new CampoRepository(_db);
            var listadoCampos = campoRepository.GetAllCampos(metaData);
            
            List<int> numeradoresIds = listadoCampos.Where(c=>c.Concepto==Constantes.ConceptoNumerador)
                            .Select(c => c.NumeroDeCampo).ToList();

            var data = new List<object>();

            if (numeradoresIds?.Count > 0)
            {
                data.AddRange(_db.Query<DtoNumeratorDefinition>("SELECT * FROM NUMERATORDEFINITION WHERE NUMERO IN (@0)", numeradoresIds));
            }
            return data;
        }


    }
}
