﻿using ControlMetadata.Model.Entities;
using ControlMetadata.Topaz.Dtos;
using ControlMetadata.Topaz.Interfaces;
using ControlMetadata.ViewModel.Helpers;
using PetaPoco;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static ControlMetadata.Enumerador;

namespace ControlMetadata.Topaz.Repositories
{
    class OperacionRepository : ITopazRepository
    {

        private Database _db;

        /// <summary>
        /// Constructor de la clase
        /// </summary>
        /// <param name="db"></param>
        public OperacionRepository(Database db)
        {
            _db = db;
        }

        /// <summary>
        /// Metodo principal para recuperar los registros de TopazDB
        /// </summary>
        /// <param name="metaData">Registros en la base Metadata</param>
        /// <returns></returns>
        public List<object> GetData(List<Metadata> metaData)
        {
            var listadoIds = metaData.GetByTipo(ObjetosTopaz.Operativa).GetNumeroTopaz();

            var data = new List<object>();
            if (listadoIds?.Count > 0)
                data.AddRange(_db.Query<DtoOperacion>("SELECT * FROM OPERACIONES WHERE IDENTIFICACION IN (@0)", listadoIds).ToList());

            return data;
        }

    }
}
