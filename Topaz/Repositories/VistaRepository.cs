﻿using ControlMetadata.Model.Entities;
using ControlMetadata.Topaz.Dtos;
using ControlMetadata.Topaz.Interfaces;
using ControlMetadata.ViewModel.Helpers;
using PetaPoco;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static ControlMetadata.Enumerador;

namespace ControlMetadata.Topaz.Repositories
{
    /// <summary>
    /// Repositorio para Ayudas y Vistas
    /// </summary>
    public class VistaRepository : ITopazRepository
    {
        private Database _db;

        /// <summary>
        /// Constructor de la clase
        /// </summary>
        /// <param name="db"></param>
        public VistaRepository(Database db)
        {
            _db = db;
        }

        /// <summary>
        /// Recupera las ayudas de todos los descriptores
        /// </summary>
        /// <param name="descriptoresId">Numeros de descriptores</param>
        /// <returns></returns>
        private List<DtoAyuda> AyudasDescriptores(List<int> descriptoresId)
        {
            return _db.Query<DtoAyuda>("SELECT * FROM AYUDAS WHERE NUMERODEARCHIVO IN (@0)", descriptoresId).ToList();
        }

        /// <summary>
        /// Recupera las ayudas de todas las vistas
        /// </summary>
        /// <param name="vistasId">Numeros de vistas</param>
        /// <returns></returns>
        private List<DtoAyuda> AyudasVistas(List<int> vistasId)
        {
            return _db.Query<DtoAyuda>("SELECT * FROM AYUDAS WHERE NUMERODEAYUDA IN (@0)", vistasId).ToList();
        }


        /// <summary>
        /// Recupera la informacion de la base de datos de Topaz
        /// </summary>
        /// <param name="metaData">Metadata</param>
        /// <returns></returns>
        public List<object> GetData(List<Metadata> metaData)
        {
            var descriptoresIds = metaData.GetByTipo(ObjetosTopaz.Descriptor).GetNumeroTopaz();

            var vistasIds = metaData.GetByTipo(ObjetosTopaz.Vista).GetNumeroTopaz();

            var data = new List<object>();

            if(descriptoresIds?.Count>0)
                data.AddRange(AyudasDescriptores(descriptoresIds));
            
            if(vistasIds?.Count>0)
                data.AddRange(AyudasVistas(vistasIds));

            return data;
        }


    }
}
