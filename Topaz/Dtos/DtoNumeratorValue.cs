﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ControlMetadata.Topaz.Dtos
{
    public class DtoNumeratorValue
    {

       public int Dia { get; set; }

        public int Mes { get; set; }

        public int Anio { get; set; }

        public int Sucursal { get; set; }

        public int Numero { get; set; }

        public int Valor { get; set; }

        public int Oid { get; set; }

    }
}
