﻿namespace ControlMetadata.Topaz.Dtos
{

    /// <summary>
    /// Dto con los campos de la tabla en Topaz
    /// </summary>
    public class DtoDescriptor
    {

        public int Titulo { get; set; }

        public int Identificacion { get; set; }

        public string TipoDeArchivo { get; set; }

        public string Descripcion { get; set; }

        public int GrupoDelMapa { get; set; } = Constantes.DefaultInt;

        public string NombreFisico { get; set; }

        public int LargoDelRegistro { get; set; } = Constantes.DefaultInt;

        public string TipoDeDbms { get; set; }

        public int InicializacionDelRegistro { get; set; } = Constantes.DefaultInt;

        public string Base { get; set; }

        public string Seleccion { get; set; }

        public string Acepta_Movs_Diferido { get; set; }
        
    }

}
