﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ControlMetadata.Topaz.Dtos
{
    public class DtoCampo
    {

        public int NumeroDeCampo { get; set; }

        public string UsoDelCampo { get; set; }

        public int Referencia { get; set; }

        public string Descripcion { get; set; }

        public string Prompt { get; set; }

        public int Largo { get; set; } = Constantes.DefaultInt;

        public int Decimales { get; set; } = Constantes.DefaultInt;

        public string Edicion { get; set; }

        public int Contabiliza { get; set; } = Constantes.DefaultInt;

        public int Concepto { get; set; } = 0;

        public int Calculo { get; set; } = Constantes.DefaultInt;

        public int Validacion { get; set; } = 0;


        public int TablaDeValidacion { get; set; } = 0;

        public int TablaDeAyuda { get; set; } = 0;

        public int Opciones { get; set; } = Constantes.DefaultInt;

        public int Tabla { get; set; } = 0;

        public string Campo { get; set; }

        public int Basico { get; set; } = Constantes.DefaultInt;

        public string Mascara { get; set; }

        public int Accumulador { get; set; } = Constantes.DefaultInt;

    }
}
