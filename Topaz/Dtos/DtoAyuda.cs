﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ControlMetadata.Topaz.Dtos
{
    public class DtoAyuda
    {

        public int NumeroDeArchivo { get; set; }

        public int NumeroDeAyuda { get; set; }

        public string Descripcion { get; set; }

        public string Filtro { get; set; }

        public int MostrarTodos { get; set; } = Constantes.DefaultInt;

        public string Campos { get; set; }

        public string NombreVista { get; set; }

        public string BaseVista { get; set; }

        public string CamposVista { get; set; }

        public int AyudaGrande { get; set; } = Constantes.DefaultInt;


    }
}
