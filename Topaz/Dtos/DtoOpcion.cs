﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ControlMetadata.Topaz.Dtos
{
    public class DtoOpcion
    {

        public int NumeroDeCampo { get; set; }

        public string Idioma { get; set; }

        public string Descripcion { get; set; }

        public string OpcionInterna { get; set; }

        public string OpcionDePantalla { get; set; }

    }
}
