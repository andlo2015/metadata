﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ControlMetadata.Topaz.Dtos
{
    public class DtoOperacion
    {

        public int Titulo { get; set; }

        public int Identificacion { get; set; }

        public string Nombre { get; set; }

        public string Descripcion { get; set; }

        public string MNemotecnico { get; set; }

        public string Autorizacion { get; set; }

        public string FormularioPrincipal { get; set; }

        public int ProxOperacion { get; set; } = Constantes.DefaultInt;

        public string Estado { get; set; }

        public int Tz_Lock { get; set; }

        public int Copias { get; set; } = 0;

        public string PermiteBaja { get; set; }

        public int Suboperacion { get; set; } = Constantes.DefaultInt;

        public string ComportamientoEnCierre { get; set; }

        public string RequiereContrasena { get; set; }

        public string PermiteConcurrente { get; set; }

        public string PermiteEstadoDiferido { get; set; }

        public int Icono_Titulo { get; set; } = Constantes.DefaultInt;

        public int Estilo { get; set; } = Constantes.DefaultInt;



    }
}
