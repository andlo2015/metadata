﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ControlMetadata.Topaz.Dtos
{
    public class DtoIndice
    {

        public int NumeroDeArchivo { get; set; }

        public int NumeroDeIndice { get; set; }

        public string Descripcion { get; set; }

        public int ClavesRepetidas { get; set; }

        public int Campo1 { get; set; } = Constantes.DefaultInt;

        public int Campo2 { get; set; } = Constantes.DefaultInt;
        public int Campo3 { get; set; } = Constantes.DefaultInt;
        public int Campo4 { get; set; } = Constantes.DefaultInt;
        public int Campo5 { get; set; } = Constantes.DefaultInt;
        public int Campo6 { get; set; } = Constantes.DefaultInt;
        public int Campo7 { get; set; } = Constantes.DefaultInt;
        public int Campo8 { get; set; } = Constantes.DefaultInt;
        public int Campo9 { get; set; } = Constantes.DefaultInt;
        public int Campo10 { get; set; } = Constantes.DefaultInt;
        public int Campo11 { get; set; } = Constantes.DefaultInt;
        public int Campo12 { get; set; } = Constantes.DefaultInt;
        public int Campo13 { get; set; } = Constantes.DefaultInt;
        public int Campo14 { get; set; } = Constantes.DefaultInt;
        public int Campo15 { get; set; } = Constantes.DefaultInt;
        public int Campo16 { get; set; } = Constantes.DefaultInt;
        public int Campo17 { get; set; } = Constantes.DefaultInt;
        public int Campo18 { get; set; } = Constantes.DefaultInt;
        public int Campo19 { get; set; } = Constantes.DefaultInt;
        public int Campo20 { get; set; } = Constantes.DefaultInt;




    }
}
