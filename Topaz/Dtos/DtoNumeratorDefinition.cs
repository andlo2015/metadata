﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ControlMetadata.Topaz.Dtos
{
    public class DtoNumeratorDefinition
    {

        public int Numero { get; set; }

        public int IniVal { get; set; }

        public int Incremento { get; set; }

        public string Periodo { get; set; }

        public DateTime? UltimaInic { get; set; }

        public int Reutilizable { get; set; }

        public int Maximo { get; set; }

        public int Centralizado { get; set; }

        public string Descripcion { get; set; }

        public string Tipo { get; set; }


    }
}
