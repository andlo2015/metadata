﻿using ControlMetadata.Model.Entities;
using System.Collections.Generic;

namespace ControlMetadata.Topaz.Interfaces
{
    /// <summary>
    /// Interfaz para repositorios Topaz
    /// </summary>
    public interface ITopazRepository
    {
        /// <summary>
        /// Recupera la informacion de la base de datos de Topaz
        /// </summary>
        /// <param name="metadata">Metadata con la que debe trabajar</param>
        /// <returns></returns>
        List<object> GetData(List<Metadata> metaData);

    }
}
