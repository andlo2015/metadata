﻿using ControlMetadata.Model.Entities;
using System;
using System.Collections.Generic;
using static ControlMetadata.Enumerador;

namespace ControlMetadata.Topaz.Interfaces
{
    /// <summary>
    /// Interfaz que deben cumplir las clases que recuperan la informacion de la DB Topaz
    /// Para después poder generar las setencias SQL
    /// </summary>
    public interface ITopazMetadata
    {
        /// <summary>
        /// Listado de objetos que se van a procesar
        /// </summary>
        List<object> Data { get; }

        /// <summary>
        /// Genera la metadata de la base de datos Topaz
        /// </summary>
        /// <param name="metadata">Registro en la DB Metadata</param>
        string Generate(List<Metadata> metadata);

        /// <summary>
        /// Tipo de objeto Topaz que procesa
        /// </summary>
        ObjetosTopaz ObjectType { get; }

        /// <summary>
        /// Nombre de la tabla para la que genera la metadata
        /// </summary>
        string TableName { get; }
        
        /// <summary>
        /// Tipo de objeto con el que va a trabajar
        /// </summary>
        Type Tipo { get; } 

    }
}
